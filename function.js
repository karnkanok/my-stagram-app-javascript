//1.Pure Function ค่าจะไม่เปลี่ยนแปลง
function addPure(a,b){
    return a+b;
}
console.log('Pure f:',addPure(1,4));

// 2.Avoid side Effect ค่าจะเปลี่ยนถ้าตัวแปลมีการแก้ไข เพราะตัวแปลไปผูกค่ากับตัวแปลด้านนอก
const b = 10;
function addSide(a){
    return a+b;
}
console.log('Side:', addSide(1,2));

// 3.First class function
//3.1 Assigning a f to a variable
const addAssigning = function(a,b){
    return a+b;
}
console.log('Assigning:', addAssigning(1,3));


//3.2 Returning a f

function addReturning (a,b){
    return function(){
        return a+b;
    }
}
const addTwoNumber = addReturning(2,3);
console.log('Returning:', addTwoNumber());//ถ้าaddTwoNumberไม่ใส่ () จะ return ออกมาเป็น f


//3.3 Accepting a f as an argument รับค่า f ได้

function addNumber(a,b){
    return a+b;;
}

function addAccepting (add,a,b){
    return add(a,b);
}

console.log('Acception:',addAccepting(addNumber,1,3));


//4.Higher-Order Functions => 3.2+3.3
//4.1Return a f
//2.4Accepting af as an argument

const persons=[
    {name:"a",age:14},
    {name:"v",age:30},
    {name:"i",age:50},
    {name:"w",age:5},
];



//fillter 

//เขียนปกติ

// const kids=[];
// for(let i = 0; i < persons.length; i++){
//     const person = persons[i];
//     if (person.age <= 20){
//         kids.push(person);
//     }
// }
// console.log('kids:',kids);

//filter => เป็น Higher-Order f สามารถรับ argument เป็น f ใหม่ได้ => ทำหน้าที่วน loop รับค่าจาก person ไปใส่ใน f ใหม่
//person=> argument รับของ personts
const kids = persons.filter((person)=>{
    return person.age <= 15;
});
console.log('kids:',kids)

//map
//เขียนปกติ 
// const olderPerson =[];
// for (let i = 0; i < persons.length; i++){
//     const person = persons[i];
//     olderPerson.push({
//         ...person,
//         age: person.age * 2
//     });
// }
// console.log('Older:',olderPerson);

//ให้ map ไปวนรับค่าเข้ามาใน f

const olderPersons = persons.map(person =>({
    ...person,
    age: person.age * 2
})) ;
console.log('Older:',olderPersons)

//reduce รวมอายุ

// ปกติ
// let totalAge = 0;
// for (let i = 0; i < persons.length; i++ ){
//     const person = persons[i];
//     totalAge += person.age;
// }
// console.log('Total:',totalAge);

let totalAge = persons.reduce((age, person) => {
    return age + person.age;
}, 0);
console.log ('totalAge:',totalAge);


const personss=[
    {name:"ai",age:14},
    {name:"vi",age:30},
    {name:"ii",age:50},
    {name:"wi",age:5},
];

//forEach คล้าย for loop but can't use black and continule

//for loop ปกติ

// for (let i = 0;  i <= personss.length; i++){
//     const personl = personss[i];
//     if(personl.age > 15) continue ;
//     console.log(`Name ${personl.name}, Age ${personl.age}`);
// }

 personss.forEach(personl => 
     console.log(`Name ${personl.name}, age ${personl.age}`)
 );



// find, findIndex

//ธรรมดา หาค้นชื่อ ii
// let ii ;
// for (let i = 0; i < personss.length; i++){
//     const personl = personss[i];
//     if (personl.name === 'ii'){
//         ii = personl;
//         break;
//     }
// }
// console.log('Name ii:', ii );

const ii = personss.find((personl) => {
    return personl.name === 'ii';
});
//findIndex จะแสดงตำแน่งที่อยู่ของข้อมูลออกมา
const iiIndex = personss.findIndex((personl) => {
    return personl.name === 'ii';
});
console.log('Name ii:',ii);
console.log('Name ii:',iiIndex);


// every,some  ผลออกมา T/F

// ปกติ  หาว่ามีคนที่อายุ > 15 มั้ย
// let isKid = true;
// for (let i = 0; i <= personss.length; i++){
//     const personl = personss[i];
//     if (personl.age > 15){
//         isKid = false;
//         break;
//     }
// }
// console.log('isKis :', isKid);

//ถ้ามี T จะได้ F
const isKidEvery = personss.every((personl) => {
    return personl.age <= 15;
});
// ถ้ามี T จะได้ T
const isKidSome = personss.some((personl) => {
    return personl.age <= 15;
}); 
console.log('iskidEvery :',isKidEvery);
console.log('iskidSom :',isKidSome);


// ~~~~~~~~~~~ สร้าง เป็นของตัวเอง ไม่ควรทำนะ ~~~~~~~~~~~~~~
// arr เปลี่ยน เป็น this ได้
function myFilter(arr, callback){
const result = [];
    for (let i = 0; i < arr.length; i++){
        const element = arr[i];
        if (callback(element)){
            result.push(element);
        }
    }   
    return result;
} 
// กรณีใช้ this  = person.myFilter(persons => person.age <= 15);
const kidsS = myFilter(persons,person => person.age <= 15 );
console.log('kidsS:',kidsS)


//myMap

function myMap(arr,callback){
    const result = [];
    for (let i = 0; i < arr.length; i++){
        const element = arr[i];
        result.push(callback(element));
    }
    return result;
}
const olderPersonss = myMap(persons,person =>({
    ...person,
    age: person.age * 2
})) ;
console.log('Older:',olderPersonss)


//~~~ Clisures   ~~~~~~~~~~~~~ 
// f ลูก เรียกใช้ข้อมูลจาก f แม่ ได้

// ปกติ

// function outter(){
//     const name = 'Outer';
//     function inner(){
//         console.log('Outer name: ', name);
//     }
//     inner();
// }
// outter(); //การเรียกใช้ f 

function outter(){
    const name = 'Outer';
    return function inner(){
        console.log('Outer name: ', name);
    }
}
const innerFunction = outter();
innerFunction();


// ~~~ Data Privacy   ~~~~~~~~~~~~~ 

// ปกติ 

//เป็น F ที่สร้างมาเพื่อครอบการทำงาน เพราะเราไม่ควรเอาตัวแปรมาไว้นอก f โดยเราต้อง return f time ออกมา
function createTime(){
    let counter = 0;
    return function times(){
        counter += 1;
        console.log ('Counter: ', counter);
    }
}
//สร้าง times ขึ้นมารองรับ
const times = createTime();
times(); //เป็นการเรียกใช้งาน f ภายใน
times();
times();


// ~~~ Stateful functions    ~~~~~~~~~~~~~ 

//ปกติ 

// function addFive(a){
//     return a+5;
// }
// function addTen(a){
//     return a+10;
// }

// console.log('Add Five:', addFive(10));
// console.log('Add Ten:', addTen(10));

function createAdd(a){
    return function(b){
        return a+b;
    }
}

const addFive = createAdd(5);
const addTen = createAdd(10);

console.log('Add Five:', addFive(10));
console.log('Add Ten:', addTen(10));


// ~~~~~~~~~~~~~    Recursion    ~~~~~~~~~~~~~ 
//1.Base case
//2.Recursive case

// f เรียกใช้ตัวมันเอง

function countDown(n){
    if (n < 0) return;
    console.log('Count down: ', n);
    countDown( n-1 );
}
countDown(10);

//f(1) = 1;   Base case
//f(2) = 2 * f(1); Recursive case
//f(3) = 3 * f(2); Recursive case
//f(4) = 4 * f(3); => 24 Recursive case

function factorial(n){
    if (n===1) return 1;
   return n * factorial( n - 1 );
}
console.log('Factorial : ', factorial(4))

// ~~~  abc => cba ~~~~~~~~~~~~~~~~~`


//c => c
//bc => reverse(c) + b
//abc => reverse(cb) + a

function reverse(str){
    if(str.length === 1) return str;
    const [firstCharater] = str;  //a
    const remainingCharacters = str.substring(1); //ดึง bc
    return reverse(remainingCharacters)+firstCharater;
}
console.log('Reverse:',reverse('abcde'));