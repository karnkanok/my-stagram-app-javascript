// const name='Queen';
// let height = 175;
// let isMale = true;
// let city = null
// let bigNumber = 10n;
// console.log(name, height, isMale, city,bigNumber)

// //obj
// const person = { 
//     name:'queen',
//     height: 160,
//     city: 'Bangkok'
// }
// console.log(person.city);
// // เปลี่ยนแปลงค่า property
// // person.height = 100;

// console.log(person.height);

// //array
// const number = [1,5,10];
// console.log(number[1]);

// //add number on array
// number.push(22);
// console.log(number);

// //remove the last number in array
// number.pop();
// console.log(number);

// //add number to the front array
// number.unshift();
// console.log(number);

// //remove the front number in array
// number.shift();
// console.log(number);

// let password ='abcdefg';
// //.length  เช็คความยาวตัวอักษร
// if (password.length >= 8){
//     console.log('password is valid');
// }else{
//     console.log('password is invalid');
// }

// //function
// function cal_vat(money,vat){
//     return (money * vat) /100;
// }
// const total = cal_vat(100,7);
// console.log(total);

// for(let counter = 0; counter < 10; counter++){
//     if(counter % 2 !== 0){
//         continue; //กลับไปทำบรรนทัดบนต่อเลยไม่ทำ else

//     }
//     console.log(counter);
// }

function appendImageElem(keyword, index) {
    const imgElem = document.createElement('img');
    //${} ->  $ มาจาก + 
    imgElem.src = `https://source.unsplash.com/400x225/?${keyword}&sig=${index}`;
  
    const galleryElem = document.querySelector('.gallery');
    galleryElem.appendChild(imgElem);
  }
  
  function removePhotos() {
    const galleryElem = document.querySelector('.gallery');
    galleryElem.innerHTML = '';
  }
  
  function searchPhotos(event) {
    const keyword = event.target.value;
  
    if (event.key === 'Enter' && keyword) {
      removePhotos();
  
      for (let i = 1; i <= 9; i++) {
        appendImageElem(keyword, i);
      }
    }
  }
  
  function run() {
    const inputElem = document.querySelector('input');
.addEventListener('keydown', searchPhotos);
  }
  
  run();